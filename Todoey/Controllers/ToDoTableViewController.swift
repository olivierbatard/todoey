//
//  ViewController.swift
//  Todoey
//
//  Created by Olivier BATARD on 25/06/2019.
//  Copyright © 2019 Olivier BATARD. All rights reserved.
//

import UIKit
import CoreData

class ToDoTableViewController: UITableViewController {

    //var todoArray = ["Todo One", "Todo Two", "Todo Three","Todo Four", "Todo Five", "Todo Six","Todo Seven", "Todo Eight", "Todo Nine"]
    
    
    
    var todoArray = [Item]()
    var selectedCategory: Category? {
        didSet {
            self.loadItems()
        }
    }
//    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Item.plist")
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
//    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
//        if let items = defaults.array(forKey: "TodoListArray") as? [Item] {
//            todoArray = items
//        }
        
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        
        loadItems(with: Item.fetchRequest())
        
        self.tableView.separatorStyle = .none
    }
    
    
    //MARK - Add Item
    
    @IBAction func addItemButtonPressed(_ sender: Any) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new Item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            print("Success")

            let newItem = Item(context: self.context)
            newItem.title = textField.text!
            newItem.done = false
            newItem.itemToCategory = self.selectedCategory
            self.todoArray.append(newItem)
            
            self.saveItems()

        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new item"
            textField = alertTextField
        }
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
        
    }
    
    
    
    //MARK - Table View Delegate Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoTableCell", for: indexPath)
        
        cell.textLabel?.text = todoArray[indexPath.row].title
        
        cell.accessoryType = todoArray[indexPath.row].done ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if todoArray[indexPath.row].title == "TitleO" {
            todoArray[indexPath.row].setValue("Fuck", forKey: "title")
        }
        
        todoArray[indexPath.row].done = !todoArray[indexPath.row].done
        
        self.saveItems()
     
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func saveItems() {
        
     
        
        do {
            try context.save()
        } catch {
            print("Error encoding array, \(error)")
        }
        
        tableView.reloadData()
        
    }
    
    func loadItems(with request: NSFetchRequest<Item> = Item.fetchRequest(), predicate: NSPredicate? = nil) {
        
        let categoryPredicate = NSPredicate(format: "itemToCategory.name MATCHES %@", selectedCategory!.name!)
        
        
        if let addPredicate = predicate {
            let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, addPredicate])
            request.predicate = compoundPredicate
        } else {
            request.predicate = categoryPredicate
        }
        
        
        
        do {
            todoArray = try context.fetch(request)
        } catch {
            print("Error when fetching \(error)")
        }
        
        tableView.reloadData()
    }
    
    
}

extension ToDoTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let request: NSFetchRequest<Item> = Item.fetchRequest()
        
        
        self.loadItems(with: request, predicate: NSPredicate(format: "title CONTAINS[cd] %@", searchBar.text!))
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            self.loadItems()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
            
            
        }
    }

}

